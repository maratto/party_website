import React from 'react'
import { shallow } from 'enzyme'

import { BaseLogoIcon } from '../Logo-Icon'
import { LogoIconThemeDefault } from './Logo-Icon_theme_default'

const LogoIcon = LogoIconThemeDefault(BaseLogoIcon)

describe('Logo-Icon_theme_default', () => {
  const logoIconThemeDefault = shallow(<LogoIcon theme='default' />)

  it('has classes Logo-Icon and Logo-Icon_theme_default', () => {
    expect(logoIconThemeDefault.prop('className'))
      .toBe('Logo-Icon Logo-Icon_theme_default')
  })
})
