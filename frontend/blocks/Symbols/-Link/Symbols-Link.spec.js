import React from 'react'
import { shallow } from 'enzyme'

import Link from '../../Link'
import { SymbolsLink } from './Symbols-Link'

describe('Symbols-Link', () => {
  const symbolsLink = shallow(<SymbolsLink />)

  it('is Link', () => {
    expect(symbolsLink.is(Link)).toBe(true)
  })

  it('has class Symbols-Link', () => {
    expect(symbolsLink.prop('className')).toBe('Symbols-Link')
  })

  it('has default theme', () => {
    expect(symbolsLink.prop('theme')).toBe('default')
  })
})
