import React from 'react'
import { shallow } from 'enzyme'

import { ContactLink } from './Contact-Link'

describe('Contact-Link', () => {
  const contactLink = shallow(<ContactLink />)

  it('is <a>', () => {
    expect(contactLink.is('a')).toBe(true)
  })

  it('by default has class Contact-Link', () => {
    expect(contactLink.prop('className')).toBe('Contact-Link')
  })

  it('has undefined href by default', () => {
    expect(contactLink.prop('href')).toBeUndefined()
  })

  it('includes no children by default', () => {
    expect(contactLink.children().length).toBe(0)
  })
})

describe('Contact-Link with some extra class', () => {
  const contactLink = shallow(<ContactLink className='Class' />)

  it('has one more class now', () => {
    expect(contactLink.prop('className')).toBe('Contact-Link Class')
  })
})

describe('Contact-Link with href', () => {
  const contactLink = shallow(<ContactLink href='mailto:foo@bar.example' />)

  it('has href with given value on inner <a>', () => {
    expect(contactLink.prop('href')).toBe('mailto:foo@bar.example')
  })
})

describe('Contact-Link with children', () => {
  const contactLink = shallow(<ContactLink>foo@bar.example</ContactLink>)

  it('includes children', () => {
    expect(contactLink.children().length).toBe(1)
    expect(contactLink.text()).toBe('foo@bar.example')
  })
})
