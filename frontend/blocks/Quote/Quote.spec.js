import React from 'react'
import { shallow } from 'enzyme'

import { QuoteSource } from './-Source/Quote-Source'
import { QuoteText } from './-Text/Quote-Text'
import { Quote } from './Quote'

describe('Quote', () => {
  const quote = shallow(<Quote />)

  it('is <figure>', () => {
    expect(quote.is('figure')).toBe(true)
  })

  it('has class Quote', () => {
    expect(quote.prop('className')).toBe('Quote')
  })

  it('by default contains an empty Quote-Text', () => {
    expect(quote.contains(<QuoteText />)).toBe(true)
  })

  it('includes no Quote-Source by default', () => {
    expect(quote.find(QuoteSource).length).toBe(0)
  })
})

describe('Quote with children', () => {
  const quote = shallow(<Quote>text</Quote>)

  it('passes them to Quote-Text', () => {
    expect(quote.contains(<QuoteText>text</QuoteText>)).toBe(true)
  })
})

describe('Quote with source', () => {
  const quote = shallow(<Quote source='A. Author' />)

  it('contains Quote-Source with that source', () => {
    expect(quote.contains(<QuoteSource>A. Author</QuoteSource>)).toBe(true)
  })
})
