import React from 'react'
import { shallow } from 'enzyme'

import Heading from '../Heading'
import { ContactsDesc } from './-Desc/Contacts-Desc'
import { ContactsItem } from './-Item/Contacts-Item'
import { ContactsLabel } from './-Label/Contacts-Label'
import { Contacts } from './Contacts'

describe('Contacts', () => {
  const contacts = shallow(<Contacts />)

  it('is <div>', () => {
    expect(contacts.is('div')).toBe(true)
  })

  it('by default has class Contacts', () => {
    expect(contacts.prop('className')).toBe('Contacts')
  })

  it('by default is empty', () => {
    expect(contacts.children().length).toBe(0)
  })
})

describe('Contacts with extra classes', () => {
  const contacts = shallow(<Contacts className='Special-Contacts' />)

  it('has more classes now', () => {
    expect(contacts.prop('className'))
      .toBe('Contacts Special-Contacts')
  })
})

describe('Contacts with title', () => {
  const contacts = shallow(<Contacts withLabel title='Some Title' />)

  it('contains Contacts-Label with that title', () => {
    const label = (
      <ContactsLabel>
        <Heading>Some Title</Heading>
      </ContactsLabel>
    )

    expect(contacts.children().length).toBe(1)
    expect(contacts.containsMatchingElement(label)).toBe(true)
  })
})

describe('Contacts with title and desc', () => {
  const contacts = shallow(<Contacts withLabel title='title' desc='desc' />)

  it('contains Contacts-Label with these title and desc', () => {
    const label = (
      <ContactsLabel>
        <Heading>title</Heading>
        <ContactsDesc>desc</ContactsDesc>
      </ContactsLabel>
    )

    expect(contacts.children().length).toBe(1)
    expect(contacts.containsMatchingElement(label)).toBe(true)
  })
})

describe('Contacts with a child', () => {
  const contacts = shallow(<Contacts>a child</Contacts>)

  it('contains the child wrapped into Contact-Item', () => {
    expect(contacts.children().length).toBe(1)
    expect(contacts.contains(<ContactsItem>a child</ContactsItem>)).toBe(true)
  })
})
