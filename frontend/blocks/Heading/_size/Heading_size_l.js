import { withBemMod } from '@bem-react/core'

import { cnHeading } from '../Heading'
import './Heading_size_l.css'

export const HeadingSizeL = withBemMod(cnHeading(), { size: 'l' })
