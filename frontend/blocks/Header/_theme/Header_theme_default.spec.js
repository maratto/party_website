import React from 'react'
import { shallow } from 'enzyme'

import { Header as BaseHeader } from '../Header'
import { HeaderThemeDefault } from './Header_theme_default'

const Header = HeaderThemeDefault(BaseHeader)

describe('Header_theme_default', () => {
  const headerThemeDefault = shallow(<Header theme='default' />)

  it('has classes Header and Header_theme_default', () => {
    expect(headerThemeDefault.prop('className'))
      .toBe('Header Header_theme_default')
  })

  it('has burgerTheme with value "default"', () => {
    expect(headerThemeDefault.prop('burgerTheme')).toBe('default')
  })

  it('has default logoTheme', () => {
    expect(headerThemeDefault.prop('logoTheme')).toBe('default')
  })
})
