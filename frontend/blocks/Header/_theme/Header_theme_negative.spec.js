import React from 'react'
import { shallow } from 'enzyme'

import { Header as BaseHeader } from '../Header'
import { HeaderThemeNegative } from './Header_theme_negative'

const Header = HeaderThemeNegative(BaseHeader)

describe('Header_theme_negative', () => {
  const headerThemeNegative = shallow(<Header theme='negative' />)

  it('has classes Header and Header_theme_negative', () => {
    expect(headerThemeNegative.prop('className'))
      .toBe('Header Header_theme_negative')
  })

  it('has burgerTheme with value "negative"', () => {
    expect(headerThemeNegative.prop('burgerTheme')).toBe('negative')
  })

  it('has negative logoTheme', () => {
    expect(headerThemeNegative.prop('logoTheme')).toBe('negative')
  })
})
