import React from 'react'
import { shallow } from 'enzyme'

import { BaseMenuItem } from '../Menu-Item'
import { MenuItemActive } from './Menu-Item_active'

const MenuItem = MenuItemActive(BaseMenuItem)

describe('Menu-Item_active', () => {
  const menuItemActive = shallow(<MenuItem active />)

  it('has classes Menu-Item and Menu-Item_active', () => {
    expect(menuItemActive.prop('className'))
      .toBe('Menu-Item Menu-Item_active')
  })
})
