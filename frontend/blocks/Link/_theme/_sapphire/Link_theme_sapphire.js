import { withBemMod } from '@bem-react/core'

import { cnLink } from '../../Link'
import './Link_theme_sapphire.css'

export const LinkThemeSapphire = withBemMod(cnLink(), { theme: 'sapphire' })
