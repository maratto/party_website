import { withBemMod } from '@bem-react/core'

import { cnLink } from '../../Link'
import './Link_theme_baikal.css'

export const LinkThemeBaikal = withBemMod(cnLink(), { theme: 'baikal' })
