import React from 'react'
import { shallow } from 'enzyme'

import { Button } from './Button'
import { ButtonIcon } from './-Icon/Button-Icon'
import { ButtonText } from './-Text/Button-Text'

describe('Button', () => {
  const button = shallow(<Button />)

  it('is <button>', () => {
    expect(button.is('button')).toBe(true)
  })

  it('has class Button by default', () => {
    expect(button.prop('className')).toBe('Button')
  })

  it('by default has no Button-Icon', () => {
    expect(button.exists('.Button-Icon')).toBe(false)
  })

  it('contains a blank Button-Text by default', () => {
    expect(button.contains(<ButtonText />)).toBe(true)
  })
})

describe('Button with extra classes', () => {
  const button = shallow(<Button className='Bar Bar_baz_qux' />)

  it('appends them to <button>', () => {
    expect(button.prop('className')).toBe('Button Bar Bar_baz_qux')
  })
})

describe('Button with icon', () => {
  const button = shallow(<Button icon={<svg />} />)

  it('passes it to the Button-Icon', () => {
    expect(button.contains(<ButtonIcon><svg /></ButtonIcon>)).toBe(true)
  })
})

describe('Button with children', () => {
  const button = shallow(<Button>foo</Button>)

  it('passes them to the underlying Button-Text', () => {
    expect(button.contains(<ButtonText>foo</ButtonText>)).toBe(true)
  })
})
