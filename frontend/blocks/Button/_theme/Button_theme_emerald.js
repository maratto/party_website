import { withBemMod } from '@bem-react/core'

import { cnButton } from '../Button'
import './Button_theme_emerald.css'

export const ButtonThemeEmerald = withBemMod(cnButton(), { theme: 'emerald' })
