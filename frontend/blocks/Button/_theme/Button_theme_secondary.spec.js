import React from 'react'
import { shallow } from 'enzyme'

import { Button as BaseButton } from '../Button'
import { ButtonThemeSecondary } from './Button_theme_secondary'

const Button = ButtonThemeSecondary(BaseButton)

describe('Button_theme_secondary', () => {
  const buttonThemeSecondary = shallow(<Button theme='secondary' />)

  it('has classes Button and Button_theme_secondary', () => {
    expect(buttonThemeSecondary.prop('className'))
      .toBe('Button Button_theme_secondary')
  })
})
