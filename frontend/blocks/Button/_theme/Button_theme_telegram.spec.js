import React from 'react'
import { shallow } from 'enzyme'

import { Button as BaseButton } from '../Button'
import { ButtonThemeTelegram } from './Button_theme_telegram'
import { TelegramIcon } from './icons/Telegram'

const Button = ButtonThemeTelegram(BaseButton)

describe('Button_theme_telegram', () => {
  const buttonThemeTelegram = shallow(<Button theme='telegram' />)

  it('has classes Button and Button_theme_telegram', () => {
    expect(buttonThemeTelegram.prop('className'))
      .toBe('Button Button_theme_telegram')
  })

  it('contains an icon', () => {
    expect(buttonThemeTelegram.prop('icon')).toEqual(<TelegramIcon />)
  })
})
