import React from 'react'
import { shallow } from 'enzyme'

import { Footer } from './Footer'
import { FooterAside } from './-Aside/Footer-Aside'
import { FooterMain } from './-Main/Footer-Main'

describe('Footer', () => {
  const footer = shallow(<Footer />)

  it('is <footer>', () => {
    expect(footer.is('footer')).toBe(true)
  })

  it('has class Footer', () => {
    expect(footer.prop('className')).toBe('Footer')
  })

  it('has isPressButtonDisabled undefined by default', () => {
    expect(footer.prop('isPressButtonDisabled')).toBeUndefined()
  })

  it('contains Footer-Main', () => {
    expect(footer.containsMatchingElement(<FooterMain />)).toBe(true)
  })

  it('includes Footer-Aside', () => {
    expect(footer.containsMatchingElement(<FooterAside />)).toBe(true)
  })
})

describe('Footer with isPressButtonDisabled', () => {
  const footer = shallow(<Footer isPressButtonDisabled />)

  it('passes that to Footer-Aside', () => {
    const footerAside = <FooterAside isPressButtonDisabled />
    expect(footer.containsMatchingElement(footerAside)).toBe(true)
  })
})
