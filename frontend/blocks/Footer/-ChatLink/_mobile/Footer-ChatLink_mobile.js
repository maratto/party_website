import { withBemMod } from '@bem-react/core'

import { cnFooter } from '../../Footer'
import './Footer-ChatLink_mobile.css'

export const FooterChatLinkMobile = withBemMod(
  cnFooter('ChatLink'),
  { mobile: true }
)
