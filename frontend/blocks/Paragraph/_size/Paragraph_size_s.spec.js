import React from 'react'
import { shallow } from 'enzyme'

import { Paragraph as BaseParagraph } from '../Paragraph'
import { ParagraphSizeS } from './Paragraph_size_s'

const Paragraph = ParagraphSizeS(BaseParagraph)

describe('Paragraph_size_s', () => {
  const paragraph = shallow(<Paragraph size='s' />)

  it('has classes Paragraph and Paragraph_size_s', () => {
    expect(paragraph.prop('className')).toBe('Paragraph Paragraph_size_s')
  })
})
