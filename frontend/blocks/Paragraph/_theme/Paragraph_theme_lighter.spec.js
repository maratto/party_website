import React from 'react'
import { shallow } from 'enzyme'

import { Paragraph as BaseParagraph } from '../Paragraph'
import { ParagraphThemeLighter } from './Paragraph_theme_lighter'

const Paragraph = ParagraphThemeLighter(BaseParagraph)

describe('Paragraph_theme_lighter', () => {
  const paragraph = shallow(<Paragraph theme='lighter' />)

  it('has classes Paragraph and Paragraph_theme_lighter', () => {
    expect(paragraph.prop('className'))
      .toBe('Paragraph Paragraph_theme_lighter')
  })
})
