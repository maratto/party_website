import React from 'react'
import { shallow } from 'enzyme'

import { Page } from './Page'

describe('Page', () => {
  const page = shallow(<Page />)

  it('is <div>', () => {
    expect(page.is('div')).toBe(true)
  })

  it('has class Page by default', () => {
    expect(page.prop('className')).toBe('Page')
  })

  it('by default includes no children', () => {
    expect(page.children().length).toBe(0)
  })
})

describe('Page with additional class', () => {
  const page = shallow(<Page className='Page_with_mod' />)

  it('contains additional class', () => {
    expect(page.prop('className')).toBe('Page Page_with_mod')
  })
})

describe('Page with children', () => {
  const page = shallow(<Page><div /><div /></Page>)

  it('now includes two children', () => {
    expect(page.children().length).toBe(2)
  })
})
