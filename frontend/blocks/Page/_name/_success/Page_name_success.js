import React from 'react'
import { withBemMod } from '@bem-react/core'

import Heading from '../../../Heading'
import Link from '../../../Link'
import Paragraph from '../../../Paragraph'

import { PageContent } from '../../-Content/Page-Content'
import { PageFooter } from '../../-Footer/Page-Footer'
import { PageHeader } from '../../-Header/Page-Header'
import { cnPage } from '../../Page'

import ancapSmiley from './images/ancap-smiley.png'
import './Page_name_success.css'

const newBody = (Base, props) => (
  <Base {...props}>
    <PageHeader theme='default' activeLink='join' />
    <PageContent tag='main'>
      <div className={cnPage('Message')}>
        <img
          alt='Чёрно-жёлтый колобок в солнцезащитных очках
            показывает большой палец вверх'
          className={cnPage('Smiley')}
          src={ancapSmiley} />
        <section>
          <Heading className={cnPage('Head')} size='m' tag='h1'>
            Заявка у&nbsp;нас!
          </Heading>
          <Paragraph theme='default' size='m'>
            Мы&nbsp;свяжемся с&nbsp;вами в&nbsp;течение 10&nbsp;дней.
            Если нужно что-то изменить или уточнить в&nbsp;заявке, или если
            у&nbsp;вас возник иной вопрос&nbsp;— пишите нашему менеджеру
            по&nbsp;работе с&nbsp;заявками на&nbsp;почту {}
            <Link href='mailto:newblood@libertarian-party.ru' theme='baikal'>
              newblood@libertarian-party.ru
            </Link>
            .
          </Paragraph>
        </section>
      </div>
    </PageContent>
    <PageFooter />
  </Base>
)

export const PageNameSuccess = withBemMod(
  cnPage(),
  { name: 'success' },
  newBody
)
