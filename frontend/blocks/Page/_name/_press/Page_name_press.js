import React from 'react'
import { withBemMod } from '@bem-react/core'

import Contact from '../../../Contact'
import Contacts from '../../../Contacts'
import Heading from '../../../Heading'
import UList from '../../../UList'
import Paragraph from '../../../Paragraph'
import Symbols from '../../../Symbols'

import { PageContent } from '../../-Content/Page-Content'
import { PageFooter } from '../../-Footer/Page-Footer'
import { PageHeader } from '../../-Header/Page-Header'
import { cnPage } from '../../Page'

import boiko from './photos/boiko.jpg'
import boiko2x from './photos/boiko@2x.jpg'
import goode from './photos/goode.jpg'
import goode2x from './photos/goode@2x.jpg'
import svetov from './photos/svetov.jpg'
import svetov2x from './photos/svetov@2x.jpg'
import './Page_name_press.css'

const newBody = (Base, props) => (
  <Base {...props} >
    <PageHeader theme='default' activeLink='party' />
    <PageContent tag='main'>
      <Heading className={cnPage('Head')} size='l' tag='h1'>
        Для прессы
      </Heading>

      <Heading className={cnPage('Sub')} size='s' tag='h2'>
        Пресс-секретарь и&nbsp;спикеры
      </Heading>
      <Contacts className={cnPage('Contacts')}>
        <Contact
          name='Александр Гуд'
          phone='+7 999 549-94-95'
          photo={goode}
          photo2x={goode2x}
          position='Пресс-секретарь'
          telegram='goode108' />
        <Contact
          email='s.boiko@libertarian-party.ru'
          name='Сергей Бойко'
          phone='+7 916 127-43-41'
          photo={boiko}
          photo2x={boiko2x}
          position='Председатель'
          telegram='dsboiko' />
        <Contact
          email='svetov@libertarian-party.ru'
          name='Михаил Светов'
          photo={svetov}
          photo2x={svetov2x}
          position='Член московского отделения партии, политолог,
            автор Ютуб&#8209;канала СВТВ'
          telegram='mr_libertarian' />
      </Contacts>

      <div className={cnPage('Dyad')}>
        <div className={cnPage('AboutParty')}>
          <Heading className={cnPage('Sub')} size='s' tag='h2'>
            Коротко о&nbsp;партии
          </Heading>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Либертарианская партия России (ЛПР) возникла в&nbsp;2008&nbsp;году,
            и&nbsp;на&nbsp;апрель 2019&#8209;го в&nbsp;ней состоит 850&nbsp;членов
            и&nbsp;250&nbsp;сторонников из&nbsp;72&nbsp;регионов России.
            Мы&nbsp;не&nbsp;лидерская, а&nbsp;идеологическая партия:
            нас связывают не&nbsp;конкретные личности, а&nbsp;общие идеи.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Либертарианство&nbsp;— это система ценностей, постулирующая свободу,
            основанную на&nbsp;отсутствии агрессии и&nbsp;принуждения.
            Либертарианцы выступают за&nbsp;максимизацию личных
            и&nbsp;экономических свобод и&nbsp;минимизацию влияния государства
            на&nbsp;жизнь общества.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Мы&nbsp;регулярно проводим лекции, дебаты, участвуем в&nbsp;Форумах
            Свободных Людей, Чтениях Адама Смита, Мемориальных Конференциях
            Айн Рэнд. Организовываем митинги, в&nbsp;том числе против блокировки
            Телеграма 30&nbsp;апреля 2018, против пенсионной реформы
            и&nbsp;налогового грабежа 29&nbsp;июля 2018.
          </Paragraph>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Члены партии избирались муниципальными депутатами:
          </Paragraph>
          <UList className={cnPage('List')}>
            <Paragraph theme='default' size='m'>
              Вера Кичанова в&nbsp;Москве в&nbsp;2012&nbsp;году;
            </Paragraph>
            <Paragraph theme='default' size='m'>
              Андрей Шальнев в&nbsp;Подмосковье в&nbsp;2014&nbsp;году;
            </Paragraph>
            <Paragraph theme='default' size='m'>
              Дмитрий Максимов и&nbsp;Дмитрий Петров в&nbsp;Москве
              в&nbsp;2017&nbsp;году;
            </Paragraph>
            <Paragraph theme='default' size='m'>
              Денис Луговский, Денис Смирных и&nbsp;Евгений Топоров
              в&nbsp;Санкт-Петербурге в&nbsp;2019&nbsp;году.
            </Paragraph>
          </UList>
          <Paragraph className={cnPage('Par')} theme='default' size='m'>
            Мы&nbsp;не&nbsp;сотрудничаем с&nbsp;системной оппозицией.
            Государство отказывается регистрировать нашу партию,
            мы&nbsp;пытались четыре раза, последний в&nbsp;2015&nbsp;году.
            Наша цель&nbsp;— изменение существующей модели государственного
            устройства России мирными средствами в&nbsp;соответствии
            с&nbsp;либертарианскими ценностями.
          </Paragraph>
        </div>

        <div className={cnPage('Other')}>
          <Heading className={cnPage('Sub')} size='s' tag='h2'>
            Символика
          </Heading>
          <Symbols />
        </div>
      </div>
    </PageContent>
    <PageFooter isPressButtonDisabled />
  </Base>
)

export const PageNamePress = withBemMod(
  cnPage(),
  { name: 'press' },
  newBody
)
