import React from 'react'

import Contacts from '../../Contacts'
import Contact from '../../Contact'
import Link from '../../Link'

import akater from '../images/akater.jpg'
import akater2x from '../images/akater@2x.jpg'
import bazhenov from '../images/bazhenov.jpg'
import bazhenov2x from '../images/bazhenov@2x.jpg'
import boiko from '../images/boiko.jpg'
import boiko2x from '../images/boiko@2x.jpg'
import chinarov from '../images/chinarov.jpg'
import chinarov2x from '../images/chinarov@2x.jpg'
import conway from '../images/conway.jpg'
import conway2x from '../images/conway@2x.jpg'
import fedyukin from '../images/fedyukin.jpg'
import fedyukin2x from '../images/fedyukin@2x.jpg'
import grach from '../images/grach.jpg'
import grach2x from '../images/grach@2x.jpg'
import hohlov from '../images/hohlov.jpg'
import hohlov2x from '../images/hohlov@2x.jpg'
import karnavskiy from '../images/karnavskiy.jpg'
import karnavskiy2x from '../images/karnavskiy@2x.jpg'
import macapulina from '../images/macapulina.jpg'
import macapulina2x from '../images/macapulina@2x.jpg'
import maximov from '../images/maximov.jpg'
import maximov2x from '../images/maximov@2x.jpg'
import romanov from '../images/romanov.jpg'
import romanov2x from '../images/romanov@2x.jpg'
import shalnev from '../images/shalnev.jpg'
import shalnev2x from '../images/shalnev@2x.jpg'

export const HomeFederalCommittee = () => (
  <Contacts
    desc={
      <React.Fragment>
        основной руководящий орган, управляющий партией между съездами. {}
        <Link href='mailto:fk@libertarian-party.ru' theme='default'>
          fk@libertarian-party.ru
        </Link>
      </React.Fragment>
    }
    title='Федеральный комитет'
    withLabel>
    <Contact
      email='s.boiko@libertarian-party.ru'
      location='Москва'
      name='Сергей Бойко'
      phone='+7 916 127-43-41'
      photo={boiko}
      photo2x={boiko2x}
      position='Председатель'
      telegram='dsboiko' />
    <Contact
      location='Москва'
      name='Ярослав Конвей'
      photo={conway}
      photo2x={conway2x}
      position='Заместитель председателя'
      telegram='conway' />
    <Contact
      email='secretary@libertarian-party.ru'
      location='Москва'
      name='Игорь Романов'
      photo={romanov}
      photo2x={romanov2x}
      position='Ответственный секретарь'
      telegram='kroshkagarry' />
    <Contact
      location='Ямал'
      name='Сергей Карнавский'
      photo={karnavskiy}
      photo2x={karnavskiy2x}
      position='Федеральный комитет'
      telegram='Siergiej' />
    <Contact
      location='Самара'
      name='Борис Федюкин'
      photo={fedyukin}
      photo2x={fedyukin2x}
      position='Федеральный комитет'
      telegram='borisfedyukin' />
    <Contact
      email='akater@libertarian-party.ru'
      location='Москва'
      name='Дмитрий Нескоромный'
      photo={akater}
      photo2x={akater2x}
      position='Федеральный комитет' />
    <Contact
      email='a.shalnev@libertarian-party.ru'
      location='Московская область'
      name='Андрей Шальнев'
      photo={shalnev}
      photo2x={shalnev2x}
      position='Федеральный комитет'
      telegram='shalnev' />
    <Contact
      email='bajenof@libertarian-party.ru'
      location='Москва'
      name='Григорий Баженов'
      photo={bazhenov}
      photo2x={bazhenov2x}
      position='Федеральный комитет'
      telegram='bajenof' />
    <Contact
      location='Санкт-Петербург'
      name='Сергей Грач'
      photo={grach}
      photo2x={grach2x}
      position='Федеральный комитет'
      telegram='sgrach' />
    <Contact
      email='maximov@libertarian-party.ru'
      location='Москва'
      name='Дмитрий Максимов'
      photo={maximov}
      photo2x={maximov2x}
      position='Федеральный комитет' />
    <Contact
      email='ChinarovIS@libertarian-party.ru'
      location='Москва'
      name='Иван Чинаров'
      photo={chinarov}
      photo2x={chinarov2x}
      position='Федеральный комитет'
      telegram='ischinarov' />
    <Contact
      location='Тюмень'
      name='Василий Хохлов'
      photo={hohlov}
      photo2x={hohlov2x}
      position='Федеральный комитет'
      telegram='SiriKeeton' />
    <Contact
      email='mm@libertarian-party.ru'
      location='Санкт-Петербург'
      name='Марина Мацапулина'
      photo={macapulina}
      photo2x={macapulina2x}
      position='Федеральный комитет'
      telegram='spacehead' />
  </Contacts>
)
