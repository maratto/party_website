import React from 'react'

import Contact from '../../Contact'
import Contacts from '../../Contacts'
import Link from '../../Link'

import buryakov from '../images/buryakov.jpg'
import buryakov2x from '../images/buryakov@2x.jpg'
import ivanchikova from '../images/ivanchikova.jpg'
import ivanchikova2x from '../images/ivanchikova@2x.jpg'
import lugovskiy from '../images/lugovskiy.jpg'
import lugovskiy2x from '../images/lugovskiy@2x.jpg'
import samodurov from '../images/samodurov.jpg'
import samodurov2x from '../images/samodurov@2x.jpg'
import samovilov from '../images/samovilov.jpg'
import samovilov2x from '../images/samovilov@2x.jpg'

export const HomeControlCommission = () => (
  <Contacts
    desc={
      <React.Fragment>
        контролирует соблюдение устава, исполнение решений руководящих
        органов партии, а&nbsp;также контролирует финансовую, хозяйственную
        деятельность партии и&nbsp;её региональных отделений. {}
        <Link href='mailto:ckrk@libertarian-party.ru' theme='default'>
          ckrk@libertarian-party.ru
        </Link>
      </React.Fragment>
    }
    title='Центральная контрольно-ревизионная комиссия'
    withLabel>
    <Contact
      location='Москва'
      name='Сергей Буряков'
      photo={buryakov}
      photo2x={buryakov2x}
      position='Председатель центральной контрольно-ревизионной комиссии'
      telegram='Tolkien' />
    <Contact
      location='Москва'
      name='Дмитрий Самовилов'
      photo={samovilov}
      photo2x={samovilov2x}
      position='Центральная контрольно-ревизионная комиссия'
      telegram='WillDonut' />
    <Contact
      location='Москва'
      name='Мария Иванчикова'
      photo={ivanchikova}
      photo2x={ivanchikova2x}
      position='Центральная контрольно-ревизионная комиссия'
      telegram='mariaivo' />
    <Contact
      email='samodurov@libertarian-party.ru'
      location='Москва'
      name='Кирилл Самодуров'
      phone='+7 926 416-02-89'
      photo={samodurov}
      photo2x={samodurov2x}
      position='Центральная контрольно-ревизионная комиссия'
      telegram='kir_sam' />
    <Contact
      location='Санкт-Петербург'
      name='Денис Луговский'
      photo={lugovskiy}
      photo2x={lugovskiy2x}
      position='Центральная контрольно-ревизионная комиссия'
      telegram='delugov' />
  </Contacts>
)
