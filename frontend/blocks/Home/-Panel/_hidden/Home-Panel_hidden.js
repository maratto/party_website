import { withBemMod } from '@bem-react/core'

import { cnHome } from '../../Home'
import './Home-Panel_hidden.css'

export const HomePanelHidden = withBemMod(cnHome('Panel'), { hidden: true })
