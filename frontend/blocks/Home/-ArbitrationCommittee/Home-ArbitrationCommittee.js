import React from 'react'

import Contact from '../../Contact'
import Contacts from '../../Contacts'
import Link from '../../Link'

import kichanova from '../images/kichanova.jpg'
import kichanova2x from '../images/kichanova@2x.jpg'
import osenin from '../images/osenin.jpg'
import osenin2x from '../images/osenin@2x.jpg'
import ovsienko from '../images/ovsienko.jpg'
import ovsienko2x from '../images/ovsienko@2x.jpg'
import vyshegorodcev from '../images/vyshegorodcev.jpg'
import vyshegorodcev2x from '../images/vyshegorodcev@2x.jpg'

export const HomeArbitrationCommittee = () => (
  <Contacts
    desc={
      <React.Fragment>
        орган для апелляции при исключении из&nbsp;партии, отказе
        во&nbsp;вступлении, а&nbsp;также необоснованных решений Федерального
        комитета партии, включая председателя. {}
        <Link href='mailto:etcom@libertarian-party.ru' theme='default'>
          etcom@libertarian-party.ru
        </Link>
      </React.Fragment>
    }
    title='Этический комитет'
    withLabel>
    <Contact
      email='osenin@libertarian-party.ru'
      location='Москва'
      name='Владимир Осенин'
      photo={osenin}
      photo2x={osenin2x}
      position='Председатель этического комитета'
      telegram='osenin' />
    <Contact
      location='Москва'
      name='Алексей Овсиенко'
      photo={ovsienko}
      photo2x={ovsienko2x}
      position='Этический комитет'
      telegram='Ergilion' />
    <Contact
      location='Москва'
      name='Вера Кичанова'
      photo={kichanova}
      photo2x={kichanova2x}
      position='Этический комитет'
      telegram='Kichanova' />
    <Contact
      location='Челябинск'
      name='Марат Вышегородцев'
      photo={vyshegorodcev}
      photo2x={vyshegorodcev2x}
      position='Этический комитет' />
  </Contacts>
)
