import React from 'react'
import { shallow } from 'enzyme'

import { Social as BaseSocial } from '../Social'
import { SocialMediumTelegram } from './Social_medium_telegram'

const Social = SocialMediumTelegram(BaseSocial)

describe('Social_medium_telegram', () => {
  const socialMediumTelegram = shallow(<Social medium='telegram' />)

  it('has classes Social and Social_medium_telegram', () => {
    expect(socialMediumTelegram.prop('className'))
      .toBe('Social Social_medium_telegram')
  })

  it('contains an icon', () => {
    expect(socialMediumTelegram.prop('icon')).toBeTruthy()
  })
})
