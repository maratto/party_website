# frozen_string_literal: true

namespace :deploy do
  desc 'Restart the app'
  task :restart do
    on roles(:app) do
      execute "/usr/local/bin/#{fetch(:application)}ctl restart"
    end
  end

  after :finished, :restart
end
