# frozen_string_literal: true

namespace :deploy do
  namespace :credentials do
    desc 'Create credentials key file from environment variable'
    task :create_key do
      key_path = key_path_for_env(fetch(:rails_env))
      var_name = "RAILS_#{fetch(:rails_env).upcase}_KEY"
      key = ENV[var_name]

      abort "Can't create #{key_path}: #{var_name} is unset" unless key

      File.open(key_path, 'w', 0o600) do |file|
        file << key
      end
    end

    desc 'Upload credentials key file to server'
    task :upload_key do
      key_path = key_path_for_env(fetch(:rails_env))
      key_remote_absolute_path = File.join(shared_path, key_path)

      invoke 'deploy:credentials:create_key' unless File.exist?(key_path)

      on roles(:app) do
        if test("[ ! -f #{key_remote_absolute_path} ]")
          upload!(key_path, key_remote_absolute_path)
        end
      end
    end
  end

  before 'check:linked_files', 'credentials:upload_key'
end

def key_path_for_env(env)
  File.join('config', 'credentials', "#{env}.key")
end
