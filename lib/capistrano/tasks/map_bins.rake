# frozen_string_literal: true

task :map_bins do
  exec_prefix = "/usr/local/bin/#{fetch(:application)}-exec --"
  commands = %i[bundle gem rake ruby]

  commands.each do |cmd|
    SSHKit.config.command_map.prefix[cmd].prepend(exec_prefix)
  end
end

tiers = Capistrano::DSL.stages
tiers.each do |tier|
  after tier, :map_bins
end
