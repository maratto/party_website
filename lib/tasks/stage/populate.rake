# frozen_string_literal: true

# Tasks in this file are stage-only.
return unless Rails.env.staging?

DUMP_DIR = File.join(Dir.tmpdir, 'pwprod_dump')

namespace :party_website do
  namespace :stage do
    namespace :populate do
      # Populate database.
      task db: :environment do
        load_prod_db_dump
        create_dummy_user
        transfer_posts_authorship_to_dummy_user
      end

      # Populate attachments storage.
      task :attachments do
        attachments = Dir.glob(File.join(DUMP_DIR, 'storage', '*'))
        FileUtils.cp_r(attachments, Rails.root.join('storage'))
      end
    end

    desc 'Populate the stage with data found in Dir.tmpdir/pwprod_dump'
    task populate: %i[populate:db populate:attachments]
  end
end

def load_prod_db_dump
  fnames = Dir[File.join(DUMP_DIR, 'db', '*.csv')]

  fnames.each do |fname|
    table = File.basename(fname, '.csv')
    load_table_from_file(table, fname)
  end
end

def load_table_from_file(table, filename)
  conn = ActiveRecord::Base.connection.raw_connection
  sql_copy = "COPY #{conn.quote_ident(table)} FROM STDIN (FORMAT 'csv', HEADER)"

  File.open(filename, 'r') do |file|
    conn.copy_data(sql_copy) do
      while (row = file.gets)
        conn.put_copy_data(row)
      end
    end
  end
end

def create_dummy_user
  pass = Rails.application.credentials.dummy_user_password

  user_factory = ValidUserFactory.new(
    admin: true,
    email: 'stage@email.example',
    name: 'Неизвестный автор',
    password: pass
  )

  user = user_factory.produce
  user.save
end

def transfer_posts_authorship_to_dummy_user
  dummy_user_id = User.last.id
  Post.update_all(user_id: dummy_user_id)
end
