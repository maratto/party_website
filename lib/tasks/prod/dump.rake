# frozen_string_literal: true

# Tasks in this file are prod-only.
return unless Rails.env.production?

DUMP_DIR = File.join(Dir.tmpdir, 'pwprod_dump')

# Do not add users, invites, join_requests or other tables
# with sensitive data to this list.
TABLES_TO_DUMP = %w[
  active_storage_attachments
  active_storage_blobs
  federal_subjects
  illustrations
  pictures
  posts
  regional_branches
  regional_branches_telegram_chats
  regional_branches_trello_lists
  telegram_chats
  trello_lists
].freeze

namespace :party_website do
  namespace :prod do
    namespace :dump do
      # Dump database.
      task db: :environment do
        dir = mkdir_p_world_readable(File.join(DUMP_DIR, 'db'))

        TABLES_TO_DUMP.each do |table|
          fname = "#{File.join(dir, table)}.csv"
          dump_table_to_file(table, fname)
        end
      end

      # Dump attachments storage.
      task :attachments do
        dir = mkdir_p_world_readable(DUMP_DIR)
        FileUtils.cp_r(Rails.root.join('storage'), dir)
      end

      desc 'Remove Dir.tmpdir/pwprod_dump'
      task :clear do
        FileUtils.remove_entry_secure(DUMP_DIR, true)
      end
    end

    desc 'Dump production database and file storage to Dir.tmpdir/pwprod_dump'
    task dump: %i[dump:db dump:attachments]
  end
end

def mkdir_p_world_readable(name)
  FileUtils.mkdir_p(name, mode: 0o755).first
end

def dump_table_to_file(table, filename)
  conn = ActiveRecord::Base.connection.raw_connection
  sql_copy = "COPY #{table} TO STDOUT (FORMAT 'csv', HEADER)"

  File.open(filename, 'wb') do |file|
    conn.copy_data(sql_copy) do
      while (row = conn.get_copy_data)
        file << row
      end
    end
  end
end
