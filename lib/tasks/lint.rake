# frozen_string_literal: true

namespace :party_website do
  namespace :lint do
    desc 'Lint Ruby code with RuboCop'
    task :ruby do
      sh 'bin/bundle exec rubocop --parallel'
    end

    desc 'Auto-correct RuboCop offenses'
    task 'ruby:fix' do
      sh 'bin/bundle exec rubocop --safe-auto-correct'
    end

    desc 'Lint JS code with Standard'
    task :js do
      file_patterns = [
        'babel.config.js',
        "'config/webpack/*.js'",
        "'frontend/**/*.js'",
        'postcss.config.js'
      ]

      sh "bin/yarn standard --env jest #{file_patterns.join(' ')}"
    end
  end

  desc 'Run all linters'
  task lint: %i[lint:ruby lint:js]
end
