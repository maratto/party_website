# frozen_string_literal: true

namespace :party_website do
  namespace :audit do
    desc 'Run Brakeman scanner'
    task :brakeman do
      require 'brakeman'
      require 'brakeman/commandline'

      Brakeman::Commandline.start app_path: '.'
    end

    desc 'Update ruby-advisory-db and run bundle-audit'
    task :bundler do
      require 'bundler/audit/cli'

      %i[update check].each do |cmd|
        Bundler::Audit::CLI.start [cmd]
      end
    end
  end

  desc 'Scan for potential vulnerabilities'
  task audit: %i[audit:brakeman audit:bundler]
end
