# frozen_string_literal: true

# Don't seed test database.
if Rails.env.test?
  warn <<~MSG
    Warning: an attempt to seed the test database has been prevented.
    Please initialize each test with the required data explicitly.
  MSG
  return
end

# Create a user for development to browse /admin and stuff.
if Rails.env.development?
  user_factory = ValidUserFactory.new(
    admin: true,
    name: 'Dev User',
    email: 'dev@email.example',
    password: 'password'
  )

  user = user_factory.produce
  user.save
end

# Create or update federal subjects.
fs_yml = File.read(Rails.root.join('db', 'seeds', 'federal_subjects.yml'))

YAML.safe_load(fs_yml).each do |params|
  fs = FederalSubject.find_or_initialize_by(name: params['name'])
  fs.code = params['code']
  fs.iso3166_2 = params['iso3166_2']
  fs.save!
end
