# frozen_string_literal: true

class CreateTelegramChats < ActiveRecord::Migration[6.0]
  def change
    create_table :telegram_chats, id: :uuid do |t|
      t.string :external_id
      t.datetime :created_at
      t.datetime :updated_at
    end

    create_table :regional_branches_telegram_chats, id: false do |t|
      t.references :regional_branch, type: :uuid
      t.references :telegram_chat, type: :uuid
    end

    remove_column(:regional_branches, :telegram_chat, :string)
  end
end
