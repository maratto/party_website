# frozen_string_literal: true

class RenameMembershipRequestsToJoinRequests < ActiveRecord::Migration[6.0]
  def change
    rename_table :membership_requests, :join_requests
  end
end
