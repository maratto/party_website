# frozen_string_literal: true

class AddISO3166Part2ToFederalSubjects < ActiveRecord::Migration[6.0]
  def change
    add_column :federal_subjects, :iso3166_2, :string
  end
end
