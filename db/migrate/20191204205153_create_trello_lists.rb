# frozen_string_literal: true

class CreateTrelloLists < ActiveRecord::Migration[6.0]
  def change
    create_table :trello_lists, id: :uuid do |t|
      t.string :external_id
      t.datetime :created_at
      t.datetime :updated_at
    end

    create_table :regional_branches_trello_lists, id: false do |t|
      t.references :regional_branch, type: :uuid
      t.references :trello_list, type: :uuid
    end

    remove_column(:regional_branches, :trello_list, :string)
  end
end
