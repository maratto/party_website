# frozen_string_literal: true

class AddCodeToFederalSubjects < ActiveRecord::Migration[6.0]
  def change
    add_column(:federal_subjects, :code, :integer, limit: 2)
  end
end
