# frozen_string_literal: true

FactoryBot.define do
  factory :trello_list do
    sequence(:external_id) { |n| "beef#{n.to_s(16)}" }
  end
end
