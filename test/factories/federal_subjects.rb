# frozen_string_literal: true

FactoryBot.define do
  factory :federal_subject do
    sequence(:code)
    sequence(:iso3166_2, 'RU-AAA')
    sequence(:name) { |n| "Регион №#{n}" }
  end
end
