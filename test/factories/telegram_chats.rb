# frozen_string_literal: true

FactoryBot.define do
  factory :telegram_chat do
    sequence(:external_id) { |n| "123#{n}" }
  end
end
