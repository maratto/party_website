# frozen_string_literal: true

require 'test_helper'

class ValidUserFactoryTest < ActiveSupport::TestCase
  def test_produces_valid_user_by_default
    factory = ValidUserFactory.new
    user = factory.produce

    assert user.valid?
  end

  def test_email_can_be_provided
    some_email = 'some@email.example'
    factory = ValidUserFactory.new(email: some_email)
    user = factory.produce

    assert user.valid?
    assert_equal some_email, user.email
  end

  def test_name_can_be_passed
    some_name = 'Some Name'
    factory = ValidUserFactory.new(name: some_name)
    user = factory.produce

    assert user.valid?
    assert_equal some_name, user.name
  end

  def test_password_can_be_specified
    some_pass = 'let me in'
    factory = ValidUserFactory.new(password: some_pass)
    user = factory.produce

    assert user.valid?
    assert_equal some_pass, user.password
  end

  def test_admin_can_be_set
    factory = ValidUserFactory.new(admin: true)
    user = factory.produce

    assert user.valid?
    assert user.admin?
  end

  def test_setting_invite
    some_invite = Invite.new
    factory = ValidUserFactory.new(invite: some_invite)
    user = factory.produce

    assert user.valid?
    assert_same some_invite, user.invite
  end

  def test_passing_encrypted_password_and_salt
    enc_pass = '$2a$10$1Txz4k2VrJF7uTErRAasHeNPJTWOTDdBAgl5Iskwg4fvqyZv/9Cuq'
    salt = 'SDFKMaYo8z_-4dursmHv'
    factory = ValidUserFactory.new(encrypted_password: enc_pass, salt: salt)
    user = factory.produce

    assert user.valid?
    assert_equal enc_pass, user.encrypted_password
    assert_equal salt, user.salt
  end

  def test_multiple_valid_users
    factory = ValidUserFactory.new
    first_user = factory.produce
    second_user = factory.produce

    assert first_user.save
    assert second_user.valid?
  end
end
