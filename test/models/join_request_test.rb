# frozen_string_literal: true

require 'test_helper'

class JoinRequestValidationsTest < ActiveSupport::TestCase
  def test_full_name_presence_validation
    join_request = build(:join_request, full_name: '')
    refute join_request.valid?
  end

  def test_if_blank_birthdate_is_fine
    join_request = build(:join_request, birthdate: nil)
    refute join_request.valid?
  end

  def test_when_birthdate_is_not_actually_date
    join_request = build(:join_request, birthdate: 'not date')
    refute join_request.valid?
  end

  def test_that_birthdate_in_future_is_not_okay
    join_request = build(:join_request, birthdate: Date.tomorrow)
    refute join_request.valid?
  end

  def test_join_request_without_phone
    join_request = build(:join_request, phone: '')
    refute join_request.valid?
  end

  def test_with_phone_in_wrong_format
    join_request = build(:join_request, phone: '8 (123) 456-78-90')
    refute join_request.valid?
  end

  def test_that_federal_subject_is_required
    join_request = build(:join_request, federal_subject_id: nil)
    refute join_request.valid?
  end

  def test_if_residence_presence_is_checked
    join_request = build(:join_request, residence: '')
    refute join_request.valid?
  end

  def test_that_blank_email_is_allowed
    join_request = build(:join_request, email: '')
    assert join_request.valid?
  end

  def test_join_request_with_wrong_email_format
    join_request = build(:join_request, email: '@email.example')
    refute join_request.valid?
  end
end

class JoinRequestTest < ActiveSupport::TestCase
  %i[
    comment
    district
    email
    full_name
    phone
    occupation
    public_organizations
    residence
    telegram
  ].each do |attr|
    define_method(:"test_#{attr}_is_stripped") do
      jr = build(:join_request, attr => ' string with spaces  ')
      assert_equal 'string with spaces', jr.send(attr)
    end

    define_method(:"test_nil_as_#{attr}") do
      jr = JoinRequest.new(attr => nil)
      assert_nil jr.send(attr)
    end

    define_method(:"test_number_as_#{attr}") do
      jr = JoinRequest.new(attr => 42)
      assert_equal '42', jr.send(attr)
    end
  end

  def test_telegram_is_normalized
    jr = build(:join_request, telegram: 'https://t.me/telegram')
    assert_equal 'telegram', jr.telegram
  end
end
