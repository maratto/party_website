# frozen_string_literal: true

require 'test_helper'

class TrelloListTest < ActiveSupport::TestCase
  def test_without_external_id
    list = build(:trello_list, external_id: '')
    refute list.valid?
  end

  def test_with_used_external_id
    existing_list = create(
      :trello_list,
      external_id: '560bf48efe2771efe9b45997'
    )

    list = build(:trello_list, external_id: existing_list.external_id)
    refute list.valid?
  end

  def test_have_no_regional_branches
    list = build(:trello_list)
    assert_empty list.regional_branches
  end

  def test_add_regional_branch
    regional_branch = RegionalBranch.new

    list = build(:trello_list)
    list.regional_branches << regional_branch

    assert_equal 1, list.regional_branches.length
    assert_includes list.regional_branches, regional_branch
  end
end
