# frozen_string_literal: true

require 'test_helper'

class RegionalBranchTest < ActiveSupport::TestCase
  def test_have_no_trello_lists
    branch = RegionalBranch.new
    assert_empty branch.trello_lists
  end

  def test_add_trello_lists
    trello_list = build(:trello_list)

    branch = RegionalBranch.new
    branch.trello_lists << trello_list

    assert_equal 1, branch.trello_lists.length
    assert_includes branch.trello_lists, trello_list
  end

  def test_have_no_telegram_chats
    branch = RegionalBranch.new
    assert_empty branch.telegram_chats
  end

  def test_add_telegram_chat
    telegram_chat = build(:telegram_chat)

    branch = RegionalBranch.new
    branch.telegram_chats << telegram_chat

    assert_equal 1, branch.telegram_chats.length
    assert_includes branch.telegram_chats, telegram_chat
  end
end
