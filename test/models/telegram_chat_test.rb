# frozen_string_literal: true

require 'test_helper'

class TelegramChatTest < ActiveSupport::TestCase
  def test_without_external_id
    chat = build(:telegram_chat, external_id: '')
    refute chat.valid?
  end

  def test_existing_external_id
    existing_chat = create(:telegram_chat, external_id: '395373859')
    chat = build(:telegram_chat, external_id: existing_chat.external_id)
    refute chat.valid?
  end

  def test_have_no_regional_branches
    chat = build(:telegram_chat)
    assert_empty chat.regional_branches
  end

  def test_add_regional_branch
    regional_branch = RegionalBranch.new

    chat = build(:telegram_chat)
    chat.regional_branches << regional_branch

    assert_equal 1, chat.regional_branches.length
    assert_includes chat.regional_branches, regional_branch
  end
end
