# frozen_string_literal: true

require 'test_helper'

class UserTest < ActiveSupport::TestCase
  def test_if_user_is_valid_without_a_name
    user = build(:user, name: '')
    refute user.valid?
  end

  def test_user_with_existing_name
    existing_user = create(:user, name: 'Some Name')
    user = build(:user, name: existing_user.name)
    refute user.valid?
  end

  def test_email_presence_validation
    user = build(:user, email: '')
    refute user.valid?
  end

  def test_that_email_is_downcased
    user = build(:user, email: 'Email@Example.com')
    user.validate
    assert_equal 'email@example.com', user.email
  end

  def test_that_not_unique_email_is_rejected
    existing_user = create(:user, email: 'some@email.example')
    user = build(:user, email: existing_user.email.capitalize)
    refute user.valid?
  end

  def test_without_password_and_confirmation
    user = build(:user, password: nil, password_confirmation: nil)
    refute user.valid?
  end

  def test_too_short_password
    user = build(:user, password: 'foo')
    refute user.valid?
  end

  def test_blank_password_confirmation
    user = build(:user, password: 'valid pass')

    user.password_confirmation = ''
    refute user.valid?

    user.password_confirmation = nil
    refute user.valid?
  end

  def test_not_matching_passwords
    user = build(:user, password: 'some pass')
    user.password_confirmation = user.password.reverse
    refute user.valid?
  end

  def test_encrypted_password_and_salt
    enc_pass = '$2a$10$1Txz4k2VrJF7uTErRAasHeNPJTWOTDdBAgl5Iskwg4fvqyZv/9Cuq'
    salt = 'SDFKMaYo8z_-4dursmHv'
    user = build(
      :user,
      password: nil,
      password_confirmation: nil,
      encrypted_password: enc_pass,
      salt: salt
    )

    assert user.valid?
    assert_equal enc_pass, user.encrypted_password
    assert_equal salt, user.salt
  end

  def test_that_invite_is_required
    user = build(:user, invite: nil)
    refute user.valid?
  end

  def test_that_two_users_cannot_share_one_invite
    invite = build(:invite)
    create(:user, invite: invite)

    user = build(:user, invite: invite)
    refute user.valid?
  end

  def test_admin
    user = build(:user, admin: true)
    assert user.valid?
  end
end
