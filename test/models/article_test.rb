# frozen_string_literal: true

require 'test_helper'

class ArticleTest < ActiveSupport::TestCase
  def setup
    @article = Article.new
  end

  def test_no_predefined_author
    assert_nil @article.author
  end

  def test_type_is_article
    assert_equal 'Article', @article.type
  end

  def test_published_eh
    refute @article.published?
  end
end

class ArticleSaveTest < ActiveSupport::TestCase
  def setup
    @article = build(:article)
  end

  def test_save
    assert @article.save
  end

  def test_without_author
    @article.author = nil
    refute @article.save
  end
end

class PublishedArticleTest < ActiveSupport::TestCase
  def setup
    @article = build(:article, :published)
  end

  def test_published_eh
    assert @article.published?
  end
end

class PublishedArticleSaveTest < ActiveSupport::TestCase
  def setup
    @article = build(:article, :published)
  end

  def test_save
    assert @article.save
  end

  def test_with_blank_annotation
    [nil, '', ' '].each do |blank_value|
      @article.annotation = blank_value
      refute @article.save
    end
  end

  def test_with_too_long_annotation
    @article.annotation = 'A' * 141
    refute @article.save
  end

  def test_with_blank_body
    [nil, '', ' '].each do |blank_value|
      @article.body = blank_value
      refute @article.save
    end
  end

  def test_without_published_at
    @article.published_at = nil
    refute @article.save
  end

  def test_without_thumbnail
    @article.thumbnail = nil
    refute @article.save
  end

  def test_with_blank_title
    [nil, '', ' '].each do |blank_value|
      @article.title = blank_value
      refute @article.save
    end
  end
end
