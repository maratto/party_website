# frozen_string_literal: true

require 'test_helper'

class FederalSubjectValidationsTest < ActiveSupport::TestCase
  def test_name_is_required
    fs = build(:federal_subject, name: nil)
    refute fs.valid?
  end

  def test_name_must_be_unique
    existing = create(:federal_subject, name: 'Калининград')
    fs = build(:federal_subject, name: existing.name.upcase)
    refute fs.valid?
  end

  def test_code_is_required
    fs = build(:federal_subject, code: nil)
    refute fs.valid?
  end

  def test_string_code_is_invalid
    fs = build(:federal_subject, code: 'not a number')
    refute fs.valid?
  end

  def test_float_code_is_invalid
    fs = build(:federal_subject, code: 12.9)
    refute fs.valid?
  end

  def test_code_must_not_be_negative
    fs = build(:federal_subject, code: -1)
    refute fs.valid?
  end

  def test_code_must_be_unique
    existing = create(:federal_subject, code: 1)
    fs = build(:federal_subject, code: existing.code)
    refute fs.valid?
  end

  def test_iso_code_can_be_nil
    fs = build(:federal_subject, iso3166_2: nil)
    assert fs.valid?
  end

  def test_iso_code_must_be_unique
    existing = create(:federal_subject, iso3166_2: 'RU-AAA')
    fs = build(:federal_subject, iso3166_2: existing.iso3166_2)
    refute fs.valid?
  end

  def test_iso_code_of_wrong_format
    %w[foo UR-AAA ru-aaa RU-AAAA _RU-AAA RU-AA1].each do |str|
      fs = build(:federal_subject, iso3166_2: str)
      refute fs.valid?
    end
  end

  def test_iso_code_of_right_format
    %w[RU-AB RU-XYZ].each do |str|
      fs = build(:federal_subject, iso3166_2: str)
      assert fs.valid?
    end
  end
end

class FederalSubjectTest < ActiveSupport::TestCase
  def test_other_returns_federal_subject_with_code_zero_if_present
    create(:federal_subject, code: 2)
    create(:federal_subject, code: 0)
    create(:federal_subject, code: 1)

    other = FederalSubject.other
    assert 0, other.code
  end

  def test_other_returns_nil_when_no_federal_subject_with_code_zero
    create(:federal_subject, code: 1)
    other = FederalSubject.other
    assert_nil other
  end

  def test_other_is_listed_in_all
    create(:federal_subject, code: 0)
    create(:federal_subject, code: 1)

    all = FederalSubject.all
    other = FederalSubject.other

    assert_includes all, other
  end

  def test_all_but_other_exclude_other
    create(:federal_subject, code: 0)
    create(:federal_subject, code: 1)

    all_but_other = FederalSubject.all_but_other
    other = FederalSubject.other

    refute_includes all_but_other, other
  end
end
