# frozen_string_literal: true

require 'test_helper'

class StatementTest < ActiveSupport::TestCase
  def setup
    @statement = Statement.new
  end

  def test_no_predefined_author
    assert_nil @statement.author
  end

  def test_type_is_statement
    assert_equal 'Statement', @statement.type
  end

  def test_published_eh
    refute @statement.published?
  end
end

class StatementSaveTest < ActiveSupport::TestCase
  def setup
    @statement = build(:statement)
  end

  def test_save
    assert @statement.save
  end

  def test_without_author
    @statement.author = nil
    refute @statement.save
  end
end

class PublishedStatementTest < ActiveSupport::TestCase
  def setup
    @statement = build(:statement, :published)
  end

  def test_published_eh
    assert @statement.published?
  end
end

class PublishedStatementSaveTest < ActiveSupport::TestCase
  def setup
    @statement = build(:statement, :published)
  end

  def test_save
    assert @statement.save
  end

  def test_with_blank_annotation
    [nil, '', ' '].each do |blank_value|
      @statement.annotation = blank_value
      refute @statement.save
    end
  end

  def test_with_too_long_annotation
    @statement.annotation = 'A' * 141
    refute @statement.save
  end

  def test_with_blank_body
    [nil, '', ' '].each do |blank_value|
      @statement.body = blank_value
      refute @statement.save
    end
  end

  def test_without_published_at
    @statement.published_at = nil
    refute @statement.save
  end

  def test_without_thumbnail
    @statement.thumbnail = nil
    refute @statement.save
  end

  def test_with_blank_title
    [nil, '', ' '].each do |blank_value|
      @statement.title = blank_value
      refute @statement.save
    end
  end
end
