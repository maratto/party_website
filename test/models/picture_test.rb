# frozen_string_literal: true

require 'test_helper'

class PictureTest < ActiveSupport::TestCase
  def test_if_attachment_is_mandatory
    picture = build(:picture)
    picture.file = nil
    refute picture.valid?
  end

  def test_if_text_file_can_be_attached
    picture = build(:picture)

    picture.file.attach(
      io: File.open(__FILE__),
      filename: 'picture_test.rb',
      content_type: 'text/plain'
    )

    refute picture.valid?
  end
end
