# frozen_string_literal: true

require 'test_helper'

class PostTest < ActiveSupport::TestCase
  def test_that_post_has_no_type_by_default
    assert_nil Post.new.type
  end

  def test_post_with_empty_type
    post = Post.new
    post.validate
    assert post.errors.added?(:type, :inclusion, value: nil)
  end

  def test_that_post_itself_is_not_in_the_types_list
    refute Post.types.include?('Post')
  end

  def test_type_not_included_in_the_types_list
    post = Post.new
    post.type = 'Post'
    post.validate
    assert post.errors.added?(:type, :inclusion, value: 'Post')
  end

  def test_type_not_corresponding_to_the_class_name
    post = Post.new
    post.type = Post.types.sample
    post.validate
    assert post.errors.added?(:type, :not_equal_to_class)
  end
end
