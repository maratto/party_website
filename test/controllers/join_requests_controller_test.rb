# frozen_string_literal: true

require 'test_helper'

class JoinRequestsControllerTest < ActionDispatch::IntegrationTest
  def test_new
    get new_join_request_url
    assert_response :success
  end

  def test_create
    params = {
      join_request: build(:join_request).attributes
    }

    assert_difference('JoinRequest.count') do
      post join_requests_url, params: params
    end
    assert_redirected_to success_url
  end

  def test_that_mailer_is_called_if_email_is_present
    params = {
      join_request: build(:join_request).attributes
    }
    params[:join_request][:email] = 'some@email.example'

    assert_difference('ActionMailer::Base.deliveries.size') do
      post join_requests_url, params: params
    end

    mail = ActionMailer::Base.deliveries.last
    assert_equal [params[:join_request][:email]], mail.to
  end

  def test_that_mailer_is_not_called_without_email_provided
    params = {
      join_request: build(:join_request).attributes
    }

    assert_no_difference('ActionMailer::Base.deliveries.size') do
      post join_requests_url, params: params
    end
  end

  def test_that_mailer_is_not_called_unless_record_saved
    params = {
      join_request: JoinRequest.new.attributes
    }

    assert_no_difference('ActionMailer::Base.deliveries.size') do
      post join_requests_url, params: params
    end
  end
end
