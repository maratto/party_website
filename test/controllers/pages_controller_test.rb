# frozen_string_literal: true

require 'test_helper'

class PagesControllerTest < ActionDispatch::IntegrationTest
  def test_donate
    get donate_url
    assert_response :success
  end

  def test_libertarianism
    get libertarianism_url
    assert_response :success
  end

  def test_party
    get party_url
    assert_response :success
  end

  def test_press
    get press_url
    assert_response :success
  end

  def test_success
    get success_url
    assert_response :success
  end
end
