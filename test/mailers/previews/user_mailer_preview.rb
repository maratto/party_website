# frozen_string_literal: true

class UserMailerPreview < ActionMailer::Preview
  def reset_password
    UserMailer.reset_password(User.first)
  end
end
