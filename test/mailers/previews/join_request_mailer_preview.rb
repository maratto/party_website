# frozen_string_literal: true

class JoinRequestMailerPreview < ActionMailer::Preview
  def follow_up
    JoinRequestMailer.follow_up(JoinRequest.first)
  end
end
