# frozen_string_literal: true

require 'test_helper'

class JoinRequestMailerTest < ActionMailer::TestCase
  def setup
    @join_request = build(
      :join_request,
      email: 'foobar@email.example',
      full_name: 'Full Name'
    )
  end

  def test_follow_up_email_delivery
    mail = JoinRequestMailer.follow_up(@join_request)

    assert_emails 1 do
      mail.deliver_now
    end
  end

  def test_follow_up_email_headers
    mail = JoinRequestMailer.follow_up(@join_request)
    mail.deliver_now

    assert_equal ['noreply@testhost'], mail.from
    assert_equal [@join_request.email], mail.to
    assert_equal I18n.t('join_request_mailer.follow_up.subject'),
                 mail.subject
  end

  def test_follow_up_email_body
    mail = JoinRequestMailer.follow_up(@join_request)
    mail.deliver_now

    assert_equal read_fixture('follow_up.html').join, mail.html_part.body.to_s
    assert_equal read_fixture('follow_up.txt').join, mail.text_part.body.to_s
  end
end
