# frozen_string_literal: true

require 'test_helper'

class FactoriesTest < ActiveSupport::TestCase
  FactoryBot.factories.each do |factory|
    define_method "test_#{factory.name}_factory_produces_valid_objects" do
      assert create(factory.name).valid?
      assert build(factory.name).valid?
    end
  end
end
