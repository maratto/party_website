# frozen_string_literal: true

source 'https://rubygems.org'

ruby '~> 2.7.0'

gem 'bulma-rails', '~> 0.5.3'
gem 'kaminari', '~> 1.1.0'
gem 'pg', '~> 0.18'
gem 'puma', '~> 3.7'
gem 'rails', '~> 6.0.0'
gem 'react-rails', '~> 2.4.7'
gem 'ruby-trello', '~> 2.1'
gem 'sass-rails', '~> 5.0'
gem 'sorcery', '~> 0.11.0'
gem 'turbolinks', '~> 5'
gem 'uglifier', '>= 1.3.0'
gem 'webpacker', '~> 4.0'

group :development do
  gem 'brakeman', '~> 4.7', require: false
  gem 'bundler-audit', '~> 0.6', require: false
  gem 'capistrano-rails', '~> 1.3', require: false
  gem 'rubocop', '~> 0.75', require: false
end

group :development, :test do
  gem 'capybara', '~> 2.13'
  gem 'dotenv', '~> 2.5'
  gem 'factory_bot_rails', '~> 4.8.2'
  gem 'poltergeist', '~> 1.16'
end

group :staging, :production do
  gem 'mini_racer', '~> 0.2.6'
end
