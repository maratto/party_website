# frozen_string_literal: true

set(:project, 'party_website')
set(:repo_url, "https://gitlab.com/libertarian-party/#{fetch(:project)}.git")

# Use another vocabulary. These are tiers, not stages.
set(:tier, proc { fetch(:stage) })

set(:user, proc { [fetch(:project), fetch(:tier)].join('_') })
set(:deploy_to, proc { "/var/www/#{fetch(:project)}/#{fetch(:tier)}" })

dirs_to_link = %w[
  .bundle
  log
  public/assets
  storage
  tmp/cache
  tmp/pids
  tmp/sockets
]
set(:linked_dirs, dirs_to_link)
set(:linked_files, proc { ["config/credentials/#{fetch(:rails_env)}.key"] })

# capistrano-rails specific:
set(:keep_assets, 4)
set(:migration_role, :app)
