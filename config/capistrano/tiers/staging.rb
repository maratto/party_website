# frozen_string_literal: true

set(:application, 'pwstage')
set(:rails_env, 'staging')
set(:branch, 'master')

host = ENV.fetch('STAGE_DEPLOY_HOST')
server host, user: fetch(:user), roles: %w[app db web]
