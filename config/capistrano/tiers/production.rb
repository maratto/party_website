# frozen_string_literal: true

set(:application, 'pwprod')
set(:rails_env, 'production')
set(:branch, 'deployed')

host = ENV.fetch('PROD_DEPLOY_HOST')
server host, user: fetch(:user), roles: %w[app db web]
