# frozen_string_literal: true

# Settings specified here will take precedence over those
# in config/application.rb.
Rails.application.configure do
  # Debug mode disables concatenation and preprocessing of assets.
  # This option may cause significant delays in view rendering with a large
  # number of complex assets.
  config.assets.debug = true

  # Suppress logger output for asset requests.
  config.assets.quiet = true

  # Disable Action Controller fragment caching.
  config.action_controller.perform_caching = false

  # Set options that'll be used for URI generation in mailers.
  config.action_mailer.default_url_options = {
    host: ENV.fetch('DOMAIN_NAME'),
    port: ENV.fetch('PORT')
  }

  # Don't care if a mailer can't send.
  config.action_mailer.raise_delivery_errors = false

  # Raise an error on page load if there are pending migrations.
  config.active_record.migration_error = :page_load

  # Store uploaded files on the local file system
  # (see config/storage.yml for options).
  config.active_storage.service = :local

  # Print deprecation notifications to the Rails logger.
  config.active_support.deprecation = :log

  # In the development environment your application's code is reloaded on
  # every request. This slows down response time but is perfect for development
  # since you don't have to restart the web server when you make code changes.
  config.cache_classes = false

  # Don't actually store anything in cache.
  config.cache_store = :null_store

  # Show full error reports.
  config.consider_all_requests_local = true

  # Do not eager load code on boot.
  config.eager_load = false
end
