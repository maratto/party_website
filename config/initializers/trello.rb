# frozen_string_literal: true

require 'trello'

creds = Rails.application.credentials

Trello.configure do |config|
  config.developer_public_key = creds.trello_developer_public_key
  config.member_token = creds.trello_member_token
end
