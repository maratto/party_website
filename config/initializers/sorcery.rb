# frozen_string_literal: true

Rails.application.config.sorcery.submodules = %i[remember_me reset_password]

Rails.application.config.sorcery.configure do |config|
  # See lib/generators/sorcery/templates/initializer.rb file in the source code
  # of the gem for options description.

  config.not_authenticated_action = :not_logged_in

  config.user_config do |user|
    user.downcase_username_before_authenticating = true
    user.crypted_password_attribute_name = :encrypted_password
    user.reset_password_expiration_period = 1.hour
    user.reset_password_mailer = UserMailer
  end

  # This line must come after the 'user_config' part.
  config.user_class = 'User'
end
