# frozen_string_literal: true

# Set default values for email header fields.
Rails.application.config.action_mailer.default_options = {
  from: format(
    'Либертарианская Партия <noreply@%{host}>',
    host: Rails.application.config.action_mailer.default_url_options[:host]
  )
}

# Do not perform fragment caching for mailer templates.
Rails.application.config.action_mailer.perform_caching = false
