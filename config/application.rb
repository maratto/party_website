# frozen_string_literal: true

require_relative 'boot'

require 'rails'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'action_view/railtie'
require 'active_job/railtie'
require 'active_record/railtie'
require 'active_storage/engine'
require 'rails/test_unit/railtie'
require 'sprockets/railtie'

Bundler.require(*Rails.groups)
Dotenv.load('.env') if Rails.env.in?(%w[development test])

module PartyWebsite
  class Application < Rails::Application
    # Initialize configuration defaults for Rails 6.0.
    config.load_defaults 6.0

    # Don't add autoload paths to $LOAD_PATH.
    # Zeitwerk uses only absolute paths internally, and applications running
    # in :zeitwerk mode do not need require_dependency, so models, controllers,
    # jobs, etc. do not need to be in $LOAD_PATH. Setting this to false saves
    # Ruby from checking these directories when resolving require calls with
    # relative paths.
    #
    # It must be set here, because when config/initializers are loaded
    # autoload paths will already be in $LOAD_PATH.
    config.add_autoload_paths_to_load_path = false

    # Application configs can go into files in config/initializers, all .rb
    # files in that directory are automatically loaded after loading
    # the framework and any gems in your app.
    # Environment-specific settings should be placed in config/environments.
  end
end
