# frozen_string_literal: true

# Environment that Puma will run in.
environment ENV.fetch('RAILS_ENV')

# Port that Puma will listen on.
port ENV.fetch('PORT')

# Path to a file where PID will be stored.
pidfile ENV.fetch('PIDFILE', 'tmp/pids/server.pid')

# Number of workers to boot in clustered mode.
# Workers are forked web server processes. If using threads and workers together
# the concurrency of the application would be max `threads` * `workers`.
# Workers do not work on JRuby or Windows (both of which do not support
# processes).
workers_count = ENV.fetch('WEB_CONCURRENCY', 0)
workers workers_count

# Use the `preload_app!` method when specifying a `workers` number.
# This directive tells Puma to first boot the application and load code
# before forking the application. This takes advantage of Copy On Write
# process behavior so workers use less memory.
preload_app! if workers_count.to_i.positive?

# Puma serves each request in a thread from an internal thread pool.
# The threads setting takes two numbers: a minimum and maximum.
# Any libraries that use thread pools should be configured to match
# the maximum value specified for Puma.
threads ENV.fetch('RAILS_MIN_THREADS', 0), ENV.fetch('RAILS_MAX_THREADS', 16)
