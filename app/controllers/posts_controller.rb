# frozen_string_literal: true

class PostsController < ApplicationController
  def index
    @posts = Post.where(published: true)
                 .page(params[:page])
                 .per(9)
                 .reorder(published_at: :desc)
  end

  def show
    @post = Post.where(published: true).find(params[:id])
  end
end
