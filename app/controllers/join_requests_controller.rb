# frozen_string_literal: true

class JoinRequestsController < ApplicationController
  before_action :set_federal_subjects, only: %i[new create]

  def new
    @join_request = JoinRequest.new
  end

  def create
    @join_request = JoinRequest.new(join_request_params)

    if @join_request.save
      @join_request.email.present? &&
        JoinRequestMailer.follow_up(@join_request).deliver_now
      redirect_to success_url
    else
      render :new
    end
  end

  private

  def set_federal_subjects
    @federal_subjects = cache(:federal_subjects, expires_in: 12.hours) do
      other = [FederalSubject.other].compact
      FederalSubject.alphabetized.all_but_other + other
    end
  end

  def join_request_params
    params
      .require(:join_request)
      .permit(
        :full_name,
        :email,
        :birthdate,
        :phone,
        :federal_subject_id,
        :residence,
        :district,
        :telegram,
        :occupation,
        :public_organizations,
        :comment
      )
  end
end
