# frozen_string_literal: true

class PagesController < ApplicationController
  def donate
  end

  def libertarianism
  end

  def party
  end

  def platform
  end

  def press
  end

  def success
  end
end
