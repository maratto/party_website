# frozen_string_literal: true

class JoinRequestPresenter < ApplicationPresenter
  def render
    @context.render(
      partial: 'join_requests/join_request',
      locals: {join_request: self},
      layout: false
    )
  end

  def fields
    %w[
      full_name
      phone
      telegram
      email
      federal_subject_name
      residence
      district
      birthdate
      occupation
      public_organizations
      comment
      created_at
    ]
  end

  def federal_subject_name
    federal_subject.name
  end

  def birthdate
    I18n.l(super)
  end

  def created_at
    super.utc.iso8601
  end

  def summary
    name = [full_name, phone]
    name << telegram if telegram.present?
    name.join(', ')
  end
end
