# frozen_string_literal: true

class JoinRequestMailer < ApplicationMailer
  def follow_up(join_request)
    @join_request = join_request
    mail(to: join_request.email)
  end
end
