document.addEventListener("turbolinks:load", function() {
  var burger = document.querySelector(".navbar-burger");

  if (burger !== null) {
    var menu = burger.parentElement.nextElementSibling;

    document.addEventListener("click", function(event) {
      if (event.target === burger) {
        burger.classList.toggle("is-active");
        menu.classList.toggle("is-active");
      }
    });
  }
});
