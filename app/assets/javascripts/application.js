/*
 *= require rails-ujs
 *= require turbolinks
 *
 *= require ./shared/util
 *= require_tree ./shared/buttons
 *= require_tree ./application/inputs
 *= require ./shared/init
 */
