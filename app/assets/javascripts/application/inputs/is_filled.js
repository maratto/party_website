/* globals Util */

document.addEventListener("turbolinks:load", function () {
  var toggleClassOnValuePresence = function (el, klass, value) {
    !!value ? el.classList.add(klass) : el.classList.remove(klass);
  };

  var inputs = Util.selectAll(".form-input");

  if (inputs.length !== 0) {
    inputs.forEach(function (input) {
      toggleClassOnValuePresence(
        Util.parentWithClass("form-field", input),
        "is-filled",
        input.value);
    });

    document.addEventListener("change", function (event) {
      if (inputs.includes(event.target)) {
        toggleClassOnValuePresence(
          Util.parentWithClass("form-field", event.target),
          "is-filled",
          event.target.value);
      }
    });
  }
});

