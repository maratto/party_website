document.addEventListener("turbolinks:load", function () {
  var hideButtons = Util.selectAll("[data-hide]");

  if (hideButtons.length > 0) {
    document.addEventListener("click", function (event) {
      if (hideButtons.includes(event.target) && Util.isPrimaryButton(event)) {
        var elements = Util.selectAll(event.target.getAttribute("data-hide"));
        elements.forEach(function (el) {
          Util.hide(el);
        });
      }
    });
  }
});
