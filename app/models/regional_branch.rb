# frozen_string_literal: true

class RegionalBranch < ApplicationRecord
  belongs_to :federal_subject
  has_and_belongs_to_many :telegram_chats
  has_and_belongs_to_many :trello_lists
end
