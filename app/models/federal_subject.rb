# frozen_string_literal: true

class FederalSubject < ApplicationRecord
  has_one :regional_branch
  has_many :join_requests

  validates :code,
            uniqueness: true,
            numericality: {greater_than_or_equal_to: 0, only_integer: true}
  validates :iso3166_2,
            uniqueness: true,
            format: {with: /\ARU-[A-Z]{2,3}\z/, allow_nil: true}
  validates :name, presence: true, uniqueness: {case_sensitive: false}

  def self.alphabetized
    order(:name)
  end

  def self.all_but_other
    where.not(code: 0)
  end

  def self.other
    find_by(code: 0)
  end
end
