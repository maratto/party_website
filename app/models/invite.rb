# frozen_string_literal: true

class Invite < ApplicationRecord
  has_one :user

  def fresh?
    !expired? && !used?
  end

  def expired?
    created_at.present? && created_at < 12.hours.ago
  end

  def used?
    user.present?
  end
end
