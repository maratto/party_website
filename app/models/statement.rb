# frozen_string_literal: true

class Statement < Post
  belongs_to :author, class_name: 'User', foreign_key: 'user_id'
  has_one :thumbnail,
          class_name: 'Picture',
          as: :picturable,
          dependent: :destroy
  has_many :illustrations, foreign_key: 'post_id'

  accepts_nested_attributes_for :thumbnail

  with_options if: :published? do
    validates :annotation, presence: true, length: {maximum: 140}
    validates :body, presence: true
    validates :thumbnail, presence: true
    validates :published_at, presence: true
    validates :title, presence: true
  end
end
