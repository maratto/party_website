# frozen_string_literal: true

class JoinRequest < ApplicationRecord
  belongs_to :federal_subject

  validates :full_name, presence: true
  validates :birthdate, presence: true
  validates :phone, presence: true, format: {with: /\A\+\d{8,15}\z/}
  validates :federal_subject_id, presence: true
  validates :residence, presence: true
  validates :email, format: {with: /\A[^@]+@[^@]+\z/}, allow_blank: true

  validate :birthdate_not_in_future

  # Redefine attribute writers to strip spaces.
  %i[
    comment
    district
    email
    full_name
    phone
    occupation
    public_organizations
    residence
  ].each do |attr|
    define_method(:"#{attr}=") { |val| super(strip_spaces(val)) }
  end

  def telegram=(val)
    super(normalize_telegram(val))
  end

  private

  def birthdate_not_in_future
    return if birthdate.blank?

    errors.add(:birthdate, :in_future) if birthdate >= Date.today
  end

  def strip_spaces(val)
    val&.to_s&.strip
  end

  def normalize_telegram(val)
    strip_spaces(val)&.sub(%r{(https?://)?t.me/}, '')
  end
end
