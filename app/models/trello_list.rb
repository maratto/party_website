# frozen_string_literal: true

class TrelloList < ApplicationRecord
  has_and_belongs_to_many :regional_branches

  validates :external_id, presence: true, uniqueness: true
end
